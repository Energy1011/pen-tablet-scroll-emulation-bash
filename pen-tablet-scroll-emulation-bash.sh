#!/bin/bash
# By Energy1011 May/13/2024, https://gitlab.com/Energy1011/pen-tablet-scroll-emulation-bash
# This Bash script solves the problem of emulating scroll wheel functionality on an XP-PEN Deco tablet.
# The script allows you to scroll by pressing the SUPER key on the keyboard and detects mouse movements along the Y-axis to scroll up and down.

# IMPORTANT TO CHANGE: Device ID for the keyboard, you can check it with: xinput list
DEVICE_KEYBOARD=17 # SINO WEALTH Gaming KB (wired) 
CONTROL_KEY_FILE="/tmp/control_key_state"
echo "false" > "$CONTROL_KEY_FILE"

# Function to detect the Control key pressed
detect_control_key() {
  while true; do
    #keycode control = 37, ALT = 64, SUPER = 133
    control_pressed=$(xinput query-state $DEVICE_KEYBOARD | grep 'key\[133\]=down')
    if [ $control_pressed ]; then
        # echo "The Control key was pressed"
        echo "true" > "$CONTROL_KEY_FILE"
    else
        echo "false" > "$CONTROL_KEY_FILE"
    fi
    sleep 0.3  # Wait a short period before checking again
  done
}

# Function to detect mouse movement along the Y-axis
detect_mouse_movement() {
  while true; do
    sleep 0.03
    if [ "$(cat "$CONTROL_KEY_FILE")" = "true" ]; then
        lastY=$(xdotool getmouselocation | grep -o 'y:[0-9]*' | awk -F':' '{print $2}')
        currentY=$(xdotool getmouselocation | grep -o 'y:[0-9]*' | awk -F':' '{print $2}')
        if [ "$lastY" = "$currentY" ]; then
          continue
        fi
        if [ $lastY -gt $currentY ]; then
          diff=$(($lastY - $currentY))
          if [ "$(cat "$CONTROL_KEY_FILE")" = "false" ]; then
            continue 
          fi
          xdotool click --repeat 1 --delay 16 5
        fi 

        if [ $lastY -lt $currentY ]; then
          diff=$(($currentY - $lastY))
          if [ "$(cat "$CONTROL_KEY_FILE")" = "false" ]; then
            continue 
          fi
          xdotool click --repeat 1 --delay 16 4
        fi 
    fi
  done
}

# Run both functions in parallel
detect_control_key &
detect_mouse_movement &

