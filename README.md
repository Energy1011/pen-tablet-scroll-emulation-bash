# pen-tablet-scroll-emulation-bash
This Bash script solves the problem of emulating scroll wheel functionality on an XP-PEN Deco tablet.

The script allows you to scroll by pressing the SUPER key on the keyboard and detects mouse movements along the Y-axis to scroll up and down.

## Getting Started
The script uses `xdotool` and `xinput`.

To run the script, use the following command:

```bash
./pen-tablet-scroll-emulation-bash.sh
```

If desired, you can change the keycode to use a different keyboard key instead of the SUPER key.

And That's it 🐧